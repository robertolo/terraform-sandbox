terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  profile    = "default"
  region     = "us-west-2"
}

resource "aws_instance" "example" {
#  ami           = "ami-830c94e3"
  ami           = "ami-038e6b679579b4ec1"
  instance_type = "t2.micro"
}
